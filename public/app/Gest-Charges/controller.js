//var serverip = 'localhost:8010'

//	TORBI Ghizlane/CHETOUANI Rajae
var app = angular.module('pfaApp');


/// factory and directive
app.factory('Excel', function ($window) {
    var uri = 'data:application/vnd.ms-excel;base64,',
        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head><body><img src="file:///C:/Users/Hssaini/Desktop/final/public/app/Gest-Charges/images/Capture.JPG" align="left" class="rounded mx-auto d-block" alt="fichier Excel" width="170"><h3 align="center">UNIVERISTE MOHAMMED PREMIER Ecole Nationale des Sciences Appliquées (ENSA)</h5><table>{table}</table></body></html>',
        base64 = function (s) { return $window.btoa(unescape(encodeURIComponent(s))); },
        format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) };
    return {
        tableToExcel: function (tableId, worksheetName) {
            var table = $(tableId),
                ctx = { worksheet: worksheetName, table: table.html() },
                href = uri + base64(format(template, ctx));
            return href;
        }
    };
})

app.directive('excelExport',
    function () {
        return {
            restrict: 'A',
            scope: {
                fileName: "@",
                data: "&exportData"
            },
            replace: true,
            template: '<button class="btn btn-primary btn-ef btn-ef-3 btn-ef-3c mb-10" ng-click="download()">Exporter en Excel <i class="fa fa-download"></i></button>',
            link: function (scope, element) {

                scope.download = function () {

                    function datenum(v, date1904) {
                        if (date1904) v += 1462;
                        var epoch = Date.parse(v);
                        return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
                    };

                    function getSheet(data, opts) {
                        var ws = {};
                        var range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
                        for (var R = 0; R != data.length; ++R) {
                            for (var C = 0; C != data[R].length; ++C) {
                                if (range.s.r > R) range.s.r = R;
                                if (range.s.c > C) range.s.c = C;
                                if (range.e.r < R) range.e.r = R;
                                if (range.e.c < C) range.e.c = C;
                                var cell = { v: data[R][C] };
                                if (cell.v == null) continue;
                                var cell_ref = XLSX.utils.encode_cell({ c: C, r: R });

                                if (typeof cell.v === 'number') cell.t = 'n';
                                else if (typeof cell.v === 'boolean') cell.t = 'b';
                                else if (cell.v instanceof Date) {
                                    cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                                    cell.v = datenum(cell.v);
                                }
                                else cell.t = 's';

                                ws[cell_ref] = cell;
                            }
                        }
                        if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
                        return ws;
                    };

                    function Workbook() {
                        if (!(this instanceof Workbook)) return new Workbook();
                        this.SheetNames = [];
                        this.Sheets = {};
                    }

                    var wb = new Workbook(), ws = getSheet(scope.data());
                    /* add worksheet to workbook */
                    wb.SheetNames.push(scope.fileName);
                    wb.Sheets[scope.fileName] = ws;
                    var wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });

                    function s2ab(s) {
                        var buf = new ArrayBuffer(s.length);
                        var view = new Uint8Array(buf);
                        for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                        return buf;
                    }

                    window.saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), scope.fileName + '.xlsx');

                };

            }
        };
    }
);

///directive
app.directive('ngConfirmClick', [
    function () {
        return {
            priority: 1,
            terminal: true,
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure?";
                var clickAction = attr.ngClick;
                element.bind('click', function (event) {
                    if (window.confirm(msg)) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
    }]);



///filter
app.filter('sumOfValue', function () {
    //somme de chaque colonne 
    return function (data, key) {
        debugger;
        if (angular.isUndefined(data) || angular.isUndefined(key))

            return 0;
        // console.log("i'm before");
        var sum = 0;
        // console.log("i'm after sum=0");
        angular.forEach(data, function (v, k) {
            sum = sum + parseInt(v[key]);
        });
        return sum;
    }
});
app.filter('sumCour', function () {
    //somme de chaque colonne 
    return function (data, key) {
        debugger;
        if (angular.isUndefined(data) || angular.isUndefined(key))

            return 0;
        // console.log("i'm before");
        var sum = 0;
        // console.log("i'm after sum=0");
        angular.forEach(data, function (v, k) {
            sum = sum + parseInt(v[key]);
        });
        return sum * 1.5;
    }
});
app.filter('sumTP', function () {
    //somme de chaque colonne 
    return function (data, key) {
        debugger;
        if (angular.isUndefined(data) || angular.isUndefined(key))

            return 0;
        // console.log("i'm before");
        var sum = 0;
        // console.log("i'm after sum=0");
        angular.forEach(data, function (v, k) {
            sum = sum + parseInt(v[key]);
        });
        return sum * 0.75;
    }
});
app.filter('totalSumPriceQty', function () {
    //somme de ts les colonnes

    return function (data, key1, key2, key3) {
        if (angular.isUndefined(data) || angular.isUndefined(key1) || angular.isUndefined(key2) || angular.isUndefined(key3))
            return 0;

        var sum = 0;
        angular.forEach(data, function (v, k) {
            sum = sum + (parseInt(v[key1]) * 0.75 + parseInt(v[key2]) + parseInt(v[key3]) * 1.5);
        });
        return sum;
    }
});


///headeracceuil controller
app.controller('acceuil_Controller', function ($scope, $rootScope) {
    $scope.yes = false;

    //watch video
    $scope.yesA = function () {
        $scope.yes = true;
    };


});

///headerold controller
app.controller('g_headerController', function ($scope, $rootScope) {
	 $scope.today = new Date();
	
    $scope.importuser = false;

    //importer compte
    $scope.importcompte = function () {
        $scope.importuser = true;
    };
    $scope.annulerupload = function () {
        $scope.importuser = false;
    };


});


///charge filiere		
app.controller('filiere_chrgController', function ($scope, $timeout, Excel, $route, $rootScope, $routeParams, $http) {
	
    //sort
    $scope.orderByField = 'Semestre';
    $scope.reverseSort = false;
   

    //calcule charges
    $scope.tableaveccharge = false;
    $scope.tablesanscharge = true;

   $scope.calcule = function () {
        var isConfirmed = confirm("Attention il faut spécifier pour chaque élement de module le nombre de groupe ! Si c'est deja fait taper sur le button 'ok'");

        if (isConfirmed) {
            $scope.tableaveccharge = true;
            $scope.tablesanscharge = false;
        }
    };
	 //return to edit page from calcule
	 $scope.prev = function () {
            $scope.tableaveccharge = false;
            $scope.tablesanscharge = true;
       
    };
	 //add new user
	    $scope.addVisible = false;
    $scope.addgh = function () {//when click on the + button , it show up the form add prof
        $scope.addVisible = true;

    };
    $scope.hideAdd = function () {//when click on hide (after +), it hide the form add prof
        $scope.addVisible = false;
    };
    $scope.valider = function () {//add a new prof to the list of profs (items)
        console.log("Saving...");
        $http.post('/api/charges/', $scope.charge).then(function (response) {
            $scope.getCharges();
        });
        $scope.addVisible = false;
    };

    //show list	 
    $scope.getCharges = function () {
        $http.get('/getCharge').then(function (response) {
            $scope.charges = response.data;
            $scope.fileName = "report";
            $scope.exportData = [];
            // Headers:
            $scope.exportData.push(["Semestre", "IntutileModule", "ElementdeModule", "Coordinnateur", "Departement", "CRs", "TD", "TP", "filiere", "annee"]);
            // Data:
            angular.forEach($scope.charges, function (value, key) {
                $scope.exportData.push([value.Semestre, value.IntutileModule, value.ElementdeModule, value.Coordinnateur, value.Departement, value.CRs.total, value.TD.total, value.TP.total, value.filiere, value.annee]);
            });
        });
    };
    //print
    $scope.printToCart = function (printSectionId) {
        var innerContents = document.getElementById(printSectionId).innerHTML;
        var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
        popupWinindow.document.close();
    };
    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
        var exportHref = Excel.tableToExcel(tableId, 'WireWorkbenchDataExport');
        $timeout(function () { location.href = exportHref; }, 100); // trigger download
    };
	
	//editer TD-CRs-TP


    //inline update	 
    $scope.tmpEdit = [];//hold the tmp info of the row in editing time
    $scope.isEditable = [];//hold the statut of a table line if it's editable or not
     $scope.hide = true;

    $scope.editer = function (obj, id) {//when click on editer button, it turn the line of the table to a form
        console.log(id);
        $scope.isEditable[id] = true;
		$scope.hide = false;
        var tmp = {};
        angular.copy(obj, tmp);
        $scope.tmpEdit[id] = tmp;
    }

    $scope.confirm = function (id, obj) {//when click on confirmer(after editer), it copy the tmp edit info to the row informations
        $scope.isEditable[id] = false;
        angular.copy($scope.tmpEdit[id], obj);
        console.log(obj);

        $http.put('/api/charges/' + id, obj).then(function (response) {
            console.log("ok i'm here");
            // window.location.reload ();
        });
		$scope.hide = true;
    }

    $scope.annuler = function (id) {//when click on annuler(after editer), it turn the line form to a simple table line
        $scope.isEditable[id] = false;
		$scope.hide = true;
    }


    //delete row
    $scope.deleteCharge = function (id) {
        var id = id;
        $http.delete('/getCharge/' + id).then(function (response) {
            $scope.getCharges();
            //window.location.reload();
        });
    };

});
app.controller('archiController', function($timeout,Excel,$scope,$route,$rootScope,$routeParams,$http){

   

$scope.getarchive = function(){
    $http.get('/archive/').then(function(response){
      $scope.archives = response.data;

    
        });
  }; 

  $scope.getannee=function(id)
    {
        var id=id;
        $http.get('/Desarchivage/'+id).then(function(data) {

$scope.archive=JSON.stringify(data.data);

$http.put('/arc/'+$scope.archive).then(function(reponse) {
	 
        });
		window.alert("Vous avez désarchiver avec succes");
	 // window.location.reload();	
// $http.delete('/removearchive/'+ id).then(function(data){
       
		
		
       // });
	    
});

    };
      $scope.getniveau=function(id1,id2)
    {var id2=id2;
        var id1=id1;
        $http.get('/Desarchivage/'+id1+'/'+id2).then(function(data) {

$scope.archive=JSON.stringify(data.data);

$http.put('/arc/'+$scope.archive).then(function(reponse) {
	 
        });
// $http.delete('/removearchive/'+ id1+'/'+id2).then(function(data){
        // window.location.reload();
		
	
       // });
});
    
    };

});



///directory liste	
app.controller('chrgController', function ($timeout, Excel, $scope, $route, $rootScope, $routeParams, $http) {

 $scope.getannee=function(id)
    {
        var id=id;
        $http.get('/charge/'+id).then(function(data) {

$scope.archive=JSON.stringify(data.data);
$http.put('/api/archive/'+$scope.archive).then(function(reponse) {
        });
$http.delete('/getChargeremove/'+ id).then(function(data){
        window.location.reload();
       });
});
        
    };

    $scope.addVisible = false;
    $scope.addgh = function () {//when click on the + button , it show up the form add prof
        $scope.addVisible = true;

    };
    $scope.hideAdd = function () {//when click on hide (after +), it hide the form add prof
        $scope.addVisible = false;
    };
    $scope.valider = function () {//add a new prof to the list of profs (items)
        console.log("Saving...");
        $http.post('/api/charges/', $scope.charge).then(function (response) {
            $scope.getCharges();
        });
        $scope.addVisible = false;
    };


    //sort
    $scope.orderByField = 'Semestre';
    $scope.reverseSort = false;



    //show list	 
    $scope.getCharges = function () {
        $http.get('/getCharge').then(function (response) {
            $scope.charges = response.data;
    
            $scope.fileName = "report";
            $scope.exportData = [];
            // Headers:
            $scope.exportData.push(["Semestre", "IntutileModule", "ElementdeModule", "Coordinnateur", "Departement", "CRs", "TD", "TP", "filiere", "annee"]);
            // Data:
            angular.forEach($scope.charges, function (value, key) {
                $scope.exportData.push([value.Semestre, value.IntutileModule, value.ElementdeModule, value.Coordinnateur, value.Departement, value.CRs.total, value.TD.total, value.TP.total, value.filiere, value.annee]);
            });
        });
    };
    //print
    $scope.printToCart = function (printSectionId) {
        var innerContents = document.getElementById(printSectionId).innerHTML;
        var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
        popupWinindow.document.close();
    };
    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
        var exportHref = Excel.tableToExcel(tableId, 'WireWorkbenchDataExport');
        $timeout(function () { location.href = exportHref; }, 100); // trigger download
    };

    //editer TD


   //inline update	 
    $scope.tmpEdit = [];//hold the tmp info of the prof in editing time
    $scope.isEditable = [];//hold the statut of a table line if it's editable or not
    $scope.hide = true;

    $scope.editer = function (obj, id) {//when click on editer button, it turn the line of the table to a form
        console.log(id);
        $scope.isEditable[id] = true;
        $scope.hide = false;
        var tmp = {};
        angular.copy(obj, tmp);
        $scope.tmpEdit[id] = tmp;
    }

    $scope.confirm = function (id, obj) {//when click on confirmer(after editer), it copy the tmp edit info to the prof informations
        $scope.isEditable[id] = false;
        angular.copy($scope.tmpEdit[id], obj);
        console.log(obj);

        $http.put('/api/charges/' + id, obj).then(function (response) {
            console.log("ok i'm here");
            // window.location.reload ();
        });
		$scope.hide = true;
    }

    $scope.annuler = function (id) {//when click on annuler(after editer), it turn the line form to a simple table line
        $scope.isEditable[id] = false;
		$scope.hide = true;
    }


    //delete item
    $scope.deleteCharge = function (id) {
        var id = id;
        $http.delete('/getCharge/' + id).then(function (response) {
            $scope.getCharges();
            //window.location.reload();
        });
    };



});

///export personnal charge
app.controller('exportprofController', function ($timeout, Excel, $scope, $route, $rootScope, $routeParams, $http) {


    //show list	 
    $scope.getCharges = function () {
        $http.get('/getCharge').then(function (response) {
            $scope.charges = response.data;

            $scope.fileName = "report";
            $scope.exportData = [];
            // Headers:
            $scope.exportData.push(["Semestre", "IntutileModule", "ElementdeModule", "Coordinnateur", "Departement", "CRs", "TD", "TP", "filiere", "annee"]);
            // Data:
            angular.forEach($scope.charges, function (value, key) {
                $scope.exportData.push([value.Semestre, value.IntutileModule, value.ElementdeModule, value.Coordinnateur, value.Departement, value.CRs.total, value.TD.total, value.TP.total, value.filiere, value.annee]);
            });
        });
    };
    //print
    $scope.printToCart = function (printSectionId) {
        var innerContents = document.getElementById(printSectionId).innerHTML;
        var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
        popupWinindow.document.close();
    };
    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
        var exportHref = Excel.tableToExcel(tableId, 'WireWorkbenchDataExport');
        $timeout(function () { location.href = exportHref; }, 100); // trigger download
    };
});




app.filter('startFrom', function() {
    return function(input, start) {
       if (!input || !input.length) { return; }
        start = +start;
        return input.slice(start);
    }
});



///controller departement
app.controller('departement_chrgController', function ($timeout, Excel, $scope, $route, $rootScope, $routeParams, $http,$filter) {

//calcule charges
    $scope.tableaveccharge = false;
    $scope.tablesanscharge = true;
	
	
	 $scope.calcule = function () {
        var isConfirmed = confirm("Attention il faut spécifier pour chaque élement de module le nombre de groupe ! Si c'est deja fait taper sur le button 'ok'");

        if (isConfirmed) {
            $scope.tableaveccharge = true;
            $scope.tablesanscharge = false;
        }
    };
	 //return to edit page from calcule
 $scope.prev = function () {
            $scope.tableaveccharge = false;
            $scope.tablesanscharge = true;
       
    };

	//add new user
    $scope.addVisible = false;
    $scope.addgh = function () {//when click on the + button , it show up the form add prof
        $scope.addVisible = true;

    };
    $scope.hideAdd = function () {//when click on hide (after +), it hide the form add prof
        $scope.addVisible = false;
    };
    $scope.valider = function () {//add a new prof to the list of row (items)
        console.log("Saving...");
        $http.post('/api/charges/', $scope.charge).then(function (response) {
            $scope.getCharges();
        });
        $scope.addVisible = false;
    };

    //sort
    $scope.orderByField = 'Semestre';
    $scope.reverseSort = false;

    //show list	 
    $scope.getCharges = function () {
        $http.get('/getCharge').then(function (response) {
            $scope.charges = response.data;

            $scope.fileName = "report";
            $scope.exportData = [];
            // Headers:
            $scope.exportData.push(["Semestre", "IntutileModule", "ElementdeModule", "Coordinnateur", "Departement", "CRs", "TD", "TP", "filiere", "annee"]);
            // Data:
            angular.forEach($scope.charges, function (value, key) {
                $scope.exportData.push([value.Semestre, value.IntutileModule, value.ElementdeModule, value.Coordinnateur, value.Departement, value.CRs.total, value.TD.total, value.TP.total, value.filiere, value.annee]);
            });
        });
    };
    //print
    $scope.printToCart = function (printSectionId) {
        var innerContents = document.getElementById(printSectionId).innerHTML;
        var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
        popupWinindow.document.close();
    };
    //excel2
    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
        var exportHref = Excel.tableToExcel(tableId, 'WireWorkbenchDataExport');
        $timeout(function () { location.href = exportHref; }, 100); // trigger download
    };


    //editer TD-TP-CRs


    //inline update	 
    $scope.tmpEdit = [];//hold the tmp info of the prof in editing time
    $scope.isEditable = [];//hold the statut of a table line if it's editable or not
    $scope.hide = true;

    $scope.editer = function (obj, id) {//when click on editer button, it turn the line of the table to a form
        console.log(id);
        $scope.isEditable[id] = true;
		  $scope.hide = false;
        var tmp = {};
        angular.copy(obj, tmp);
        $scope.tmpEdit[id] = tmp;
    }

    $scope.confirm = function (id, obj) {//when click on confirmer(after editer), it copy the tmp edit info to the prof informations
        $scope.isEditable[id] = false;
        angular.copy($scope.tmpEdit[id], obj);
        console.log(obj);

        $http.put('/api/charges/' + id, obj).then(function (response) {
            console.log("ok i'm here");
            // window.location.reload ();
        });
		$scope.hide = true;
    }

    $scope.annuler = function (id) {//when click on annuler(after editer), it turn the line form to a simple table line
        $scope.isEditable[id] = false;
			$scope.hide = true;
    }


    //delete item
    $scope.deleteCharge = function (id) {
        var id = id;
        $http.delete('/getCharge/' + id).then(function (response) {
            $scope.getCharges();
            //window.location.reload();
        });
    };


});

///charge professeur		
app.controller('prof_chrgController', function ($scope, $route, $rootScope, $routeParams, $http,filterFilter) {
//button valider charge(add enseignant) to enseignats collection
// Gestion: button calcule charge + search (charge-ens-depa)
 $scope.ensei=true;
 // Gestion:affichage table (charge-ens-depa)
 $scope.val=false;
  // Gestion:search another enseignant (charge-ens-depa)
 $scope.v=false;
 //sum
  $scope.studentdata = function(search) {
    var found = filterFilter($scope.charges, {
      $: search
    });
    if (search == undefined) {
      var found = $scope.charges;
    }
    console.log(found)
    var sommetotal = 0;
    for (var i = 0; i < found.length; i++) {
      var charge = found[i];
      sommetotal += charge.totalfinalCRs* 1.5 + charge.totalfinalTD*1 + charge.totalfinalTP *0.75 ;
    }
    return sommetotal;
  }
$scope.vali = function () {	
console.log($scope.enseignan);
 var isConfirmed = confirm("Attention  la validation ne se fait qu'une fois par an, êtes vous sur de votre choix ? ");
        if (isConfirmed) {
            $http.post('/api/enseignant/', $scope.enseignan).then(function (response) {
             $scope.getCharges();
        });	}	
    };
$scope.enseign = function () {
	 $scope.ensei=true;
      $scope.v=false;
  $scope.val=false;	
    };
 $scope.sub = function () {
	  $scope.v=true;
	 $scope.ensei=false;
        $scope.getCharges();
        $scope.val=true;  
		        
    };
//show list	 
    $scope.getCharges = function () {
        $http.get('/getCharge').then(function (response) {
            $scope.charges = response.data;
            $scope.fileName = "report";
            $scope.exportData = [];
            // Headers:
            $scope.exportData.push(["Semestre", "IntutileModule", "ElementdeModule", "Coordinnateur", "Departement", "CRs", "TD", "TP", "filiere", "annee"]);
            // Data:
            angular.forEach($scope.charges, function (value, key) {
                $scope.exportData.push([value.Semestre, value.IntutileModule, value.ElementdeModule, value.Coordinnateur, value.Departement, value.CRs.total, value.TD.total, value.TP.total, value.filiere, value.annee]);
            });
        });
    };
    //print
    $scope.printToCart = function (printSectionId) {
        var innerContents = document.getElementById(printSectionId).innerHTML;
        var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
        popupWinindow.document.close();
    };
    //excel2
    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
        var exportHref = Excel.tableToExcel(tableId, 'WireWorkbenchDataExport');
        $timeout(function () { location.href = exportHref; }, 100); // trigger download
    };
//add enseignant

	$scope.addVisible = false;
	  
  
    $scope.addgh = function () {//when click on the + button , it show up the form add prof
        $scope.addVisible = true;

    };
    $scope.hideAdd = function () {//when click on hide (after +), it hide the form add prof
        $scope.addVisible = false;
    };
    $scope.valider = function () {//add a new prof to the list of profs (items)
        console.log("Saving...");
        $http.post('/api/charges/', $scope.charge).then(function (response) {
            $scope.getCharges();
        });
        $scope.addVisible = false;
    };
$scope.prev = function () {
            $scope.tableaveccharge = false;
            $scope.tablesanscharge = true;
       
    };

    //sort
    $scope.orderByField = 'ElementdeModule';
    $scope.reverseSort = false;

    //calcule charges
    $scope.tableaveccharge = false;
    $scope.tablesanscharge = true;

 //inline update	  
    $scope.tmpEdit = [];//hold the tmp info of the prof in editing time
    $scope.isEditable = [];//hold the statut of a table line if it's editable or not
    $scope.hide = true;

    $scope.annuler = function (id) {//when click on annuler(after editer), it turn the line form to a simple table line
        $scope.isEditable[id] = false;
		
		  $scope.getCharge();
    }

//button conf for enseigant-info directory (button active)
    $scope.edit = function (obj, id) {//when click on editer button, it turn the line of the table to a form
        console.log(id);
        $scope.isEditable[id] = true;
        var tmp = {};
        angular.copy(obj, tmp);
        $scope.tmpEdit[id] = tmp;
    }

    $scope.conf = function (id, obj) {//when click on confirmer(after editer), it copy the tmp edit info to the prof informations

        $scope.isEditable[id] = false;
        angular.copy($scope.tmpEdit[id], obj);
        console.log(obj.active);
 
            $http.put('/api/charges/' + id, obj).then(function (response) {
                console.log("ok i'm here");
				$scope.getCharges();
                // window.location.reload ();
            });
    }
// fin button conf for enseigant-info directory (button active)

    $scope.calcule = function () {
        var isConfirmed = confirm("Attention il faut spécifier pour chaque élement de module le nombre de groupe ! Si c'est deja fait taper sur le button 'ok'");

        if (isConfirmed) {
            $scope.tableaveccharge = true;
            $scope.tablesanscharge = false;
        }
    };
	
    //delete item
    $scope.deleteCharge = function (id) {
        var id = id;
        $http.delete('/getCharge/' + id).then(function (response) {
            // window.location.reload();


        });
    };



});

///export personnal charge
app.controller('exportdepController', function ($timeout, Excel, $scope, $route, $rootScope, $routeParams, $http) {


$scope.getTotal = function(){
    var total = 0;
    for(var i = 0; i < $scope.enseignants.length; i++){
        var product = $scope.enseignants[i];
        total += (product.totale)*1;
    }
    return total;
}
 //show list	 
    $scope.getEnseigants = function () {
        $http.get('/getEnseigant').then(function (response) {
            $scope.enseignants = response.data;

            $scope.fileName = "report";
            $scope.exportData = [];
            // Headers:
            $scope.exportData.push(["Semestre", "IntutileModule", "ElementdeModule", "Coordinnateur", "Departement", "CRs", "TD", "TP", "filiere", "annee"]);
            // Data:
            angular.forEach($scope.charges, function (value, key) {
                $scope.exportData.push([value.Semestre, value.IntutileModule, value.ElementdeModule, value.Coordinnateur, value.Departement, value.CRs.total, value.TD.total, value.TP.total, value.filiere, value.annee]);
            });
        });
    };
    //print
    $scope.printToCart = function (printSectionId) {
        var innerContents = document.getElementById(printSectionId).innerHTML;
        var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
        popupWinindow.document.close();
    };
    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
        var exportHref = Excel.tableToExcel(tableId, 'WireWorkbenchDataExport');
        $timeout(function () { location.href = exportHref; }, 100); // trigger download
    };
});


